/**
 * @module AppController
 */

import UIController from "./ui-controller.js"
import StorageController from "./storage-controller.js"
import HabitService from "./habit-service.js"

const AppController = ((uiCtrl, storageCtrl, habitService) => {
	const { createHabitForm, habitList } = uiCtrl.getDOMElements()

	const handleCreateFormSubmit = (event) => {
		event.preventDefault()
		const data = new FormData(createHabitForm)
		const habitTitle = data.get("habitTitle")
		if (!habitTitle.trim()) {
			return uiCtrl.showAlert("Please enter a habit to track")
		}

		const hasHabit = habitService.hasHabitTitle(habitTitle)

		if (hasHabit) {
			uiCtrl.showAlert(`You already have a habit named ${habitTitle}`)
			createHabitForm.reset()
			return false
		}

		habitService.add(habitTitle)

		uiCtrl.renderHabitList(habitService.get())
		storageCtrl.write(habitService.get())
		createHabitForm.reset()

		return true
	}

	const handleHabitListClick = (event) => {
		if (event.target.tagName !== "BUTTON") {
			return false
		}

		const { action, id } = event.target.dataset

		if (action === "complete") {
			habitService.complete(id)
		} else if (action === "delete") {
			habitService.delete(id)
		}

		uiCtrl.renderHabitList(habitService.get())
		storageCtrl.write(habitService.get())
		return true
	}

	const _init = () => {
		// Load From storage
		const storedHabits = storageCtrl.read()
		if (storedHabits !== null && Array.isArray(storedHabits)) {
			habitService.set(storedHabits)
			uiCtrl.renderHabitList(habitService.get())
		}
		// Event Listeners
		createHabitForm.addEventListener("submit", handleCreateFormSubmit)
		habitList.addEventListener("click", handleHabitListClick)
	}

	return {
		init: _init,
	}
})(UIController, StorageController, HabitService)

AppController.init()
