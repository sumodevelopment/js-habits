export default ((storage) => {
	const KEY = "habits"

	const _write = (data) => {
		if (!data) {
			throw new Error("ERR: Could not write storage - No data provided")
		}

		storage.setItem(KEY, JSON.stringify(data))
	}
	const _read = () => {
		const stored = storage.getItem(KEY)
		if (stored) {
			return JSON.parse(stored)
		}
		return null
	}
	return {
		write: _write,
		read: _read,
	}
})(window.localStorage)
