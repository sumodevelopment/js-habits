/**
 * @typedef {Object} Habit
 * @property {String} title - Title of the habit
 * @property {Boolean} completed - Is the Habit completed
 * @property {String} id - Unique identifier for the Habit
 */

export default (() => {
	/** @type {Array<Habit>} habits */
	const _habits = []

	/**
	 * getHabitIndexById
	 * @param String Habit ID
	 * @returns {number} Index of Habit
	 */
	const _getHabitIndexById = (id) => _habits.findIndex((habit) => habit.id === id)

	/**
	 *
	 * @param {string} habitId
	 * @returns {boolean}
	 */
	const _completeHabit = (habitId) => {
		const habitIdx = _getHabitIndexById(habitId)
		if (habitIdx === -1) {
			return false
		}
		_habits[habitIdx].completed = true
		return true
	}

	const _hasHabitTitle = (habitTitle) => _habits.find((habit) => habit.title === habitTitle.trim())

	/**
	 * @param {string} habitTitle
	 */
	const _addHabit = (habitTitle) => {
		_habits.push({
			id: Math.random().toString(16).slice(2),
			title: habitTitle,
			completed: false,
		})
		return _habits
	}

	/**
	 * @param {string} habitId
	 * @returns {boolean}
	 */
	const _deleteHabit = (habitId) => {
		const idx = _getHabitIndexById(habitId)
		_habits.splice(idx, 1)
		return true
	}

	return {
		/** @returns {Array<Habit>} habits */
		get() {
			return _habits
		},
		set(habits) {
			_habits.push(...habits)
		},
		add: _addHabit,
		delete: _deleteHabit,
		getIndex: _getHabitIndexById,
		complete: _completeHabit,
		hasHabitTitle: _hasHabitTitle,
	}
})()
