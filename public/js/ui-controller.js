export default (() => {
	const createHabitForm = document.getElementById("createHabitForm")
	const habitList = document.getElementById("habitList")
	const formError = document.getElementById("formError")
	const completionRate = document.getElementById("completionRate")

	const _createHabitListItem = (habit) => {
		const isCompleted = habit.completed ? "✔" : ""
		return `
            <li class="habit-list-item" id="${habit.id}">
                ${isCompleted} ${habit.title} 
                <aside>
                    <button data-action="complete" data-id="${habit.id}" class="habit-action">
                        <span class="material-icons">check</span>
                    </button>
                    <button data-action="delete" data-id="${habit.id}" class="habit-action">
                        <span class="material-icons">close</span>
                    </button>
                </aside>
            </li>
        `
	}

	const _renderCompletionRate = (habits) => {
		const totalHabits = habits.length
		if (habits.length === 0) {
			completionRate.innerHTML = ""
			return false
		}
		const totalCompleted = habits.filter((h) => h.completed).length
		if (totalCompleted === 0) {
			completionRate.innerHTML = `You have completed 0% of your daily habits`
			return true
		}

		const completedPercentage = Math.round((totalCompleted / totalHabits) * 100)
		completionRate.innerHTML = `You have completed ${completedPercentage}% of your daily habits`
		return true
	}

	const _renderHabitList = (habits) => {
		habitList.innerHTML = ""
		if (habits.length === 0) {
			habitList.insertAdjacentHTML("beforeend", "<li>You have no habits</li>")
		}
		const html = habits.map((habit) => _createHabitListItem(habit)).join("")
		habitList.insertAdjacentHTML("beforeend", html)
		_renderCompletionRate(habits)
	}

	const _showAlert = (message) => {
		formError.innerHTML = `
        <div class="alert">
            <b>Something is not right:</b>
            <p>${message}</p>
        </div>
        `
	}

	const _getDOMElements = () => ({
		createHabitForm,
		habitList,
	})

	return {
		getDOMElements: _getDOMElements,
		renderHabitList: _renderHabitList,
		showAlert: _showAlert,
	}
})()
