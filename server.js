import path, { join } from "path"
import express from "express"
// Initialize express
const app = express()
// Variables for server.
const { PORT = 3000 } = process.env
const __dirname = path.resolve()

app.use(express.static(join(__dirname, "public")))

app.get("/", async (req, res) => res.status(200).sendFile(join(__dirname, "public", "index.html")))

app.listen(PORT, () => console.log(`Started server on port ${PORT}`))
